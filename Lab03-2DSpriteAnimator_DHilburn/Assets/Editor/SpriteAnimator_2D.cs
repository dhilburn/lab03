﻿using UnityEngine;
using UnityEditor;
using System.Collections;


/**
 * @author Darrick Hilburn
 * 
 * This program is a tool that lets users create
 * 2D sprite animations.
 */
public class SpriteAnimator_2D : EditorWindow
{
    public static Object selectedObj;

    int numAnimations;
    string controllerName;
    string[] animationNames = new string[100];
    float[] clipFrameRate = new float[100];
    float[] clipTimeBetween = new float[100];
    int[] startFrames = new int[100];
    int[] endFrames = new int[100];
    bool[] pingPong = new bool[100];
    bool[] loop = new bool[100];

    [MenuItem("Project Tools/2D Animation")]
    public static void Init()
    {
        selectedObj = Selection.activeObject;

        if (selectedObj == null)
            EditorUtility.DisplayDialog("Selection not found!", "You must select a divided spritesheet in your assets before selecting this.", "Okay");
        else
        {
            SpriteAnimator_2D window = (SpriteAnimator_2D)EditorWindow.GetWindow(typeof(SpriteAnimator_2D));
            window.Show();
        }
    }

    void OnFocus()
    {
        if (EditorApplication.isPlayingOrWillChangePlaymode)
            this.Close();
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("Animations for " + selectedObj.name);
        EditorGUILayout.Separator();
        // Get the controller for the animations and the number of animations.
        controllerName = EditorGUILayout.TextField("Controller Name", controllerName);
        numAnimations = EditorGUILayout.IntField("Animations", numAnimations);
        // Display info for all animations in development
        for(int i = 0; i < numAnimations; i++)
        {
            // Get the animation names, start frames, and end frames.
            animationNames[i] = EditorGUILayout.TextField("Animation name", animationNames[i]);
            EditorGUILayout.BeginHorizontal();
                startFrames[i] = EditorGUILayout.IntField("Start frame", startFrames[i]);
                endFrames[i] = EditorGUILayout.IntField("End frame", endFrames[i]);
            // Get the frame rate, time between frames, and check if animat
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
                clipFrameRate[i] = EditorGUILayout.FloatField("Frame Rate", clipFrameRate[i]);
                clipTimeBetween[i] = EditorGUILayout.FloatField("Frame spacing", clipTimeBetween[i]);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
                loop[i] = EditorGUILayout.Toggle("Loop", loop[i]);
                pingPong[i] = EditorGUILayout.Toggle("Ping Pong", pingPong[i]);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }

        if(GUILayout.Button("Create"))
        {
            bool creationOkay = true;

            if(controllerName.Equals(null))
            {
                EditorUtility.DisplayDialog("ERROR", "The controller needs a name!", "Okay");
                creationOkay = false;
            }
            if (numAnimations < 1)
            {
                EditorUtility.DisplayDialog("ERROR", "There must be at least one animation!", "Okay");
            }

            if (creationOkay)
            {
                for (int i = 0; i < numAnimations; i++)
                {
                    if (animationNames[i].Equals(null))
                    {
                        EditorUtility.DisplayDialog("ERROR", "The animation needs a name!", "Okay");
                        creationOkay = false;
                        break;
                    }
                    else if (startFrames[i] < 1 || startFrames[i] > AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(selectedObj)).Length)
                    {
                        EditorUtility.DisplayDialog("ERROR", "Start frame must be in the range of sprites in the sheet!", "Okay");
                        creationOkay = false;
                        break;
                    }
                    else if (endFrames[i] < 1 || endFrames[i] > AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(selectedObj)).Length)
                    {
                        EditorUtility.DisplayDialog("ERROR", "End frame must be in the range of sprites in the sheet!", "Okay");
                        creationOkay = false;
                        break;
                    }
                    else if (startFrames[i] > endFrames[i])
                    {
                        EditorUtility.DisplayDialog("ERROR", "Start frame can't be after end frame!", "Okay");
                        creationOkay = false;
                        break;
                    }
                }
            }

            if (creationOkay)
            {
                UnityEditor.Animations.AnimatorController controller =
                    UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath("Assets/Animation Controllers/" + controllerName + ".controller");
                for (int i = 0; i < numAnimations; i++)
                {
                    AnimationClip tempClip = CreateClip(selectedObj, animationNames[i], startFrames[i], endFrames[i], clipFrameRate[i], clipTimeBetween[i], pingPong[i]);
                    if (loop[i])
                    {
                        AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(tempClip);
                        settings.loopTime = true;
                        settings.loopBlend = true;
                        AnimationUtility.SetAnimationClipSettings(tempClip, settings);
                    }
                    controller.AddMotion(tempClip);
                }
                InstantiateSpritePrefab(controller);
                this.Close();
            }
            
        }
    }

    public AnimationClip CreateClip(Object obj, string clipName, int startFrame, int endFrame, float frameRate, float timeBetween, bool pingPong)
    {
        string path = AssetDatabase.GetAssetPath(obj);
        Object[] sprites = AssetDatabase.LoadAllAssetsAtPath(path);

        int frameCount = endFrame - startFrame + 1;
        float frameLength = 1f / timeBetween;

        AnimationClip clip = new AnimationClip();
        clip.frameRate = frameRate;

        EditorCurveBinding curveBinding = new EditorCurveBinding();
        curveBinding.type = typeof(SpriteRenderer);
        curveBinding.propertyName = "m_Sprite";

        ObjectReferenceKeyframe[] keyFrames;
        if (!pingPong)
            keyFrames = new ObjectReferenceKeyframe[frameCount + 1];
        else
            keyFrames = new ObjectReferenceKeyframe[frameCount * 2 + 1];

        int frameNum = 0;

        for(int i = startFrame; i < endFrame + 1; i++, frameNum++)
        {
            ObjectReferenceKeyframe tempFrame = new ObjectReferenceKeyframe();
            tempFrame.time = frameNum * frameLength;
            tempFrame.value = sprites[i];
            keyFrames[frameNum] = tempFrame;
        }

        if(pingPong)
        {
            for(int i = endFrame; i >= startFrame; i--, frameNum++)
            {
                ObjectReferenceKeyframe tempFrame = new ObjectReferenceKeyframe();
                tempFrame.time = frameNum * frameLength;
                tempFrame.value = sprites[i];
                keyFrames[frameNum] = tempFrame;
            }
        }

        ObjectReferenceKeyframe lastSprite = new ObjectReferenceKeyframe();
        lastSprite.time = frameNum * frameLength;
        lastSprite.value = sprites[startFrame];
        keyFrames[frameNum] = lastSprite;

        clip.name = clipName;
        AnimationUtility.SetObjectReferenceCurve(clip, curveBinding, keyFrames);
        AssetDatabase.CreateAsset(clip, "Assets/Animations/" + clipName + ".anim");

        return clip;
    }

    void InstantiateSpritePrefab(UnityEditor.Animations.AnimatorController controller)
    {
        GameObject spriteObject = new GameObject();
        SpriteRenderer spriteRenderer = spriteObject.AddComponent<SpriteRenderer>();
        Animator spriteAnimator = spriteObject.AddComponent<Animator>();

        spriteObject.name = "Sprite Prefab";
        spriteObject.transform.position = Vector3.zero;
        spriteAnimator.runtimeAnimatorController = controller;

        Object spritePrefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/SpritePrefab.prefab");
        PrefabUtility.ReplacePrefab(spriteObject, spritePrefab);
        AssetDatabase.Refresh();
        DestroyImmediate(spriteObject);
        GameObject clone = PrefabUtility.InstantiatePrefab(spritePrefab) as GameObject;
    }
}
